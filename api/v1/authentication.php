<?php 
$app->get('/session', function() {
    $db = new DbHandler();
    $session = $db->getSession();
    $response["uid"] = $session['uid'];
    $response["email"] = $session['email'];
    $response["nombre"] = $session['nombre'];
    echoResponse(200, $session);
});

$app->post('/login', function() use ($app) {
    require_once 'passwordHash.php';
    require_once 'jwt_helper.php';
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('email', 'password'),$r->login);
    $response = array();
    $db = new DbHandler();
    $password = $r->login->password;
    $email = $r->login->email;

    $user = $db->getOneRecord("select id,nombre,password,email,acceso from web_users where email='$email'");
    if ($user != NULL) {
      if(passwordHash::check_password($user['password'],$password)){
                
        //ha hecho login
        $user['iat'] = time();
        $user['exp'] = time() + 2000;
        $jwt = JWT::encode($user, '');

        $response['status'] = "success";
        $response['message'] = 'Ha accesado correctamente.';
        $response['user']['nombre'] = $user['nombre'];
        $response['user']['id'] = $user['id'];
        $response['user']['email'] = $user['email'];
        $response['user']['createdAt'] = $user['acceso'];
        $response['token'] = $jwt;

      } else {
        $response['status'] = "error";
        $response['message'] = 'Login failed. Incorrect credentials';
      }
    }else {
      $response['status'] = "error";
      $response['message'] = '¡No se ha encontrado una cuenta con los datos proporcionados!';
    }
    echoResponse(200, $response);
});
$app->post('/signUp', function() use ($app) {
    $response = array();
    $r = json_decode($app->request->getBody());
    verifyRequiredParams(array('user','email','nombre','password','pais','tipo_usuario'),$r->signup);
    require_once 'passwordHash.php';
    $db = new DbHandler();
    $user = $r->signup->user;
    $password = $r->signup->password;
    $telefono = @$r->signup->telefono;
    $nombre = $r->signup->nombre;
    $email = $r->signup->email;
    $pais = $r->signup->pais;
    $tipo_usuario = $r->signup->tipo_usuario;
    $perfil = $r->signup->perfil;
    $destinos = @$r->signup->destinos;
    $informacion = @$r->signup->informacion;

    $isUserExists = $db->getOneRecord("select 1 from web_users where email='$email'");

    if(!$isUserExists){
        $r->signup->password = passwordHash::hash($password);
        $tabble_nombre = "web_users";
        $column_nombres = array('user','telefono', 'nombre', 'email', 'password', 'ciudad','pais','tipo_usuario','perfil','destinos','informacion');

        $result = $db->insertIntoTable($r->signup, $column_nombres, $tabble_nombre);
        if ($result != NULL) {

            $response["status"] = "success";
            $response["message"] = "¡La cuenta ha sido creada correctamente!";
            $response["uid"] = $result;
            echoResponse(200, $response);
        } else {
            $response["status"] = "error";
            $response["message"] = "Ha ocurrido un error al tratar de crear la cuenta. Por favor trate nuevamente";
            echoResponse(201, $response);
        }            
    }else{
        $response["status"] = "error";
        $response["message"] = "¡Un usuario con este email y/ó teléfono ya existe!";
        echoResponse(201, $response);
    }
});

$app->get('/logout', function() {
    $db = new DbHandler();
    $session = $db->destroySession();
    $response["status"] = "info";
    $response["message"] = "Se ha cerrado la sesión correctamente";
    echoResponse(200, $response);
});


$app->get('/perfil/:user', function($user) {

    $response = array();
    $db = new DbHandler();
    
    $response = $db->selectUser("web_users","*",array("id"=>$user),"1");
    echoResponse(200, $response);

});


$app->put('/editPerfil/:idUser', function($idUser) use($app) {

    $r = json_decode($app->request->getBody());
    $response = array();
    $db = new DbHandler();
    
    $response = $db->put("web_users",array("id"=>$idUser),$r->data);
    echoResponse(200, $response);

});



?>