
"use strict";


var input = 0;
/*global input*/
    angular.module('app')
        .controller('promocionesCtrl', [ '$scope', '$state','Data', '$stateParams',
            function ($scope, $state, Data, $stateParams) {

                $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                $scope.$state = $state;

				function getEncuesta(id) {
				 	Data.get('getEncuesta/' + id).then(function (results) {
				        if (results.status === "success") {
			            	$scope.preguntas = results.data.preguntas;
			            	$scope.encuestaNombre = results.data.nombre;
			            	$scope.encuestaDescripcion = results.data.descripcion;
				        }
				    });
				}

				if ($stateParams.idPro === undefined) {
				 	Data.get('promociones').then(function (results) {
				        if (results.status === "success") {
				            $scope.promociones = results.data;
				            $scope.showEncuesta = false;
				        }
				    });
				} else {
				    $scope.$parent.titulo = $stateParams.enlace;
					var idPro = $stateParams.idPro;
				 	Data.get('getPromocion/' + idPro).then(function (results) {

				        if (results.status === "success") {
				            $scope.showEncuesta = true;
			            	$scope.promociones = results.data;
				            if (results.data[0].encuesta > 0) {
				            	getEncuesta(results.data[0].encuesta);
				            };
				        }
				    });
				}

	}]);

