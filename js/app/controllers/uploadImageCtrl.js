"use strict";

angular.module('app')
.controller('uploadImageCtrl', ['$scope','CONFIG','store','$stateParams','Data', 'upload','$document', function ($scope, CONFIG, store, $stateParams,Data, upload, $document) 
{
	console.log($stateParams.id);

	var token = store.get("token") || null;
	var user = store.get("user") || null;

	console.log(user);

	$scope.src = "";
	$scope.showImage = false;

 	$scope.getImagenes  = function(idMem)
	{
		Data.get('getImagenes/' + $stateParams.id).then(function (results) {     	
        	Data.toast(results);
            if (results.status == "success") {
            	$scope.imagenes = results.data;

            }
        });
	}
	$scope.getImagenes();

 	$scope.editSubmit = function(id)
	{
		var data = {
			comentario: $scope.name
		}
		console.log(data);
		/*	*/
		Data.put('actualizarDatosImagen/' + id, {
            data
        }).then(function (results) {   	
        	Data.toast(results);
            if (results.status == "success") {
				$scope.getImagenes();
				$scope.limpiarInputs();
            }
        });
	
	}

 	$scope.guardaDatosImagen = function(src)
	{
		var imagen = {
			nombre: CONFIG.APIURL + src,
			comentario: $scope.name,
			idUser: user.id,
			idMem: $stateParams.id
		}

		if (imagen.comentario === "" || imagen.nombre === "" || imagen.idUser === "" || imagen.idMem === "") {
			alert("No has llenado todos los campos");
			return false;
		}

		Data.post('guardaDatosImagen/' + $stateParams.id, {
            imagen: imagen

        }).then(function (results) {     	
        	Data.toast(results);

            if (results.status == "success") {
				$scope.getImagenes();
				$scope.limpiarInputs();
            }
        });
	}

	$scope.uploadFile = function()
	{
		var name = $scope.name;
		var file = $scope.file;

		if (file === undefined || name === undefined) {
			alert("No has llenado todos los campos");
			return false;
		}
		
		upload.uploadFile(file, name, $stateParams.id).then(function(res) {
			Data.toast(res.data);
			
			$scope.guardaDatosImagen(res.data.src);
			console.log(res);

		})
	}


	$scope.deleteImage = function(id)
	{
		var confirma = confirm("Deseas realmente eliminar esta imagen?");
		if (confirma) {	
			Data.get('deleteImage/' + id).then(function (results) {     	
	        	Data.toast(results);

	            if (results.status == "success") {
					$scope.getImagenes();
	            }
	        });
		}
	}

	$scope.limpiarInputs = function() {

		$scope.name = "";
		$scope.src = "";
		$scope.showImage = false;
		$scope.edicion = false;

		var previewimage = angular.element($document[0].querySelector('#preview-image'))
        previewimage.addClass('hidden');
	}

	$scope.getImage = function(id) {

		$scope.edicion = true;
		Data.get('getImage/' + id).then(function (results) {     	
        	Data.toast(results);

            if (results.status == "success") {
            	$scope.name = results.data[0].comentario;
				$scope.src = results.data[0].nombre;
				$scope.showImage = true;
				$scope.id = id;
            }
        });
	}


}])

.directive('uploaderModel', ["$parse", function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, iElement, iAttrs) 
		{
			iElement.on("change", function(e)
			{
				$parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
			});
		}
	};
}])

.service('upload', ["$http", "$q", "CONFIG", function ($http, $q, CONFIG) 
{
	this.uploadFile = function(file, name, id)
	{
		var deferred = $q.defer();
		var formData = new FormData();
		formData.append("name", name);
		formData.append("file", file);


		return $http.post(CONFIG.APIURL + "uploadImagen/" +  id, formData, {
			headers: {
				"Content-type": undefined
			},
			transformRequest: angular.identity

		})
		.success(function(res)
		{
			deferred.resolve(res);
		})
		.error(function(msg, code)
		{
			deferred.reject(msg);
		})
		return deferred.promise;
	}	
}]);