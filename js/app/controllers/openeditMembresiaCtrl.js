
 "use strict";

angular.module('app')
    .controller('openeditMembresiaCtrl', function ($scope, $rootScope,$stateParams, jwtHelper, store,  $location, $http, Data) {
	
    
    $scope.$parent.titulo = "Editar propiedad";


	$scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';

	var token = store.get("token") || null;
	var user = store.get("user") || null;

	console.log(user);

	var membresia = {};


    $scope.poblateeditMembresia = function(stateParams) {

		if(user.id !== ""){
	        Data.get('poblateeditMembresia/'+stateParams.idMem).then(function(results) {

	        	console.log(results);

	            Data.toast(results);
	            if (results.status == "success") {

                	$scope.membresia = results.data[0];

	               








	               
	            }
	        });

	 	} else alert("No estas logeado");
	}
	$scope.poblateeditMembresia($stateParams);
	/* aqui traer parametro de url*/

    $scope.saveEditMembresia = function(membresia,idMem) {


		if(user.id !== ""){
	        Data.post('editMembresia/'+idMem, {
	            membresia: membresia
	        }).then(function(results) {
	        	console.log(results);
	            Data.toast(results);
	            if (results.status == "success") {
	               
	            }
	        });

	 	} else alert("No estas logeado");
	} 



})
  
    .directive('datePicker', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModel) {
                $(element).datepicker({
                    locale: 'MX',
                    format: 'yyyy-mm-dd',
                    parseInputDate: function (data) {
                        if (data instanceof Date) {
                            return moment(data);
                        } else {
                            return moment(new Date(data));
                        }
                    },
                    maxDate: new Date()
                });

                $(element).on("dp.change", function (e) {
                    ngModel.$viewValue = e.date;
                    ngModel.$commitViewValue();
                });
            }
        };
    })


.directive('datePickerInput', function() {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {
            // Trigger the Input Change Event, so the Datepicker gets refreshed
            scope.$watch(attr.ngModel, function (value) {
                if (value) {
                    element.trigger("change");
                }
            });
        }
    };
});