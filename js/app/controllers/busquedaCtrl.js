
"use strict";


var input = 0;
/*global input*/
    angular.module('app')
        .controller('busquedaCtrl', [ '$scope', '$state','Data','Busqueda','Page',
            function ($scope, $state, Data, Busqueda, Page) {

                $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                $scope.$state = $state;

                Page.setTitle($state.current.title);

                /* Paginación */
				$scope.currentPage = 0;
				$scope.pageSize = 10; // Cantidad de registros a mostrar por página
				$scope.pages = [];
				$scope.resultados = [];

				$scope.configPages = function() {
				   console.log($scope.resultados);
				   $scope.pages.length = 0;
				   var ini = $scope.currentPage - 4;
				   var fin = $scope.currentPage + 5;
				   if (ini < 1) {
				      ini = 1;
				      if (Math.ceil($scope.resultados.length / $scope.pageSize) > 10) fin = 10;
				      else fin = Math.ceil($scope.resultados.length / $scope.pageSize);
				   } else {
				      if (ini >= Math.ceil($scope.resultados.length / $scope.pageSize) - 10) {
				         ini = Math.ceil($scope.resultados.length / $scope.pageSize) - 10;
				         fin = Math.ceil($scope.resultados.length / $scope.pageSize);
				      }
				   }
				   if (ini < 1) ini = 1;
				   for (var i = ini; i <= fin; i++) {
				      $scope.pages.push({ no: i });
				   }
				   if ($scope.currentPage >= $scope.pages.length)
				      $scope.currentPage = $scope.pages.length - 1;
				};

				$scope.setPage = function(index) {
				   $scope.currentPage = index - 1;
				};

                $scope.buscador = {
                	ciudad: Busqueda.ciudad()
                };

			    $scope.busqueda = function (buscador) {

			    	if(buscador.ciudad !== undefined){
                		$scope.ciudad = buscador.ciudad;
			    	}

			        Data.post('busqueda', {
			            buscador: buscador
			        }).then(function (results) {
			            //Data.toast(results);
			            
			            if (results.status == "success") {
							$scope.configPages();
			                $scope.resultados = results;
			                $scope.total = results.total;
			            }else {

			            }
			        });
			    };

			    $scope.busqueda({ciudad: Busqueda.ciudad()});


                Data.get('publicidades').then(function (results) {
                    if (results.status === "success") {
                        $scope.publicidades = results.data;
                    }
                });

                Data.get('promociones').then(function (results) {
                    if (results.status === "success") {
                        $scope.promociones = results.data;
                    }
                });


			    
	}])
    .filter('startFromGrid', function() {

       return function(input, start) {
	       	if(input !== undefined){

	          start = +start;
	          return input.slice(start);

	       	}
       };
    });

