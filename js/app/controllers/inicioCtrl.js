"use strict";

    angular.module('app')
        .controller('inicioCtrl', [ '$scope', '$state','Data','$location','Page','Busqueda',
            function ($scope, $state, Data, $location, Page, Busqueda) {

                $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
                $scope.$state = $state;

                Page.setTitle($state.current.title);


                /*
                id  
                titulo  
                titulo_ing  
                descripcion     
                descripcion_ing     
                fecha   
                empresa     
                argumento   
                argumento_ing   
                posicion    
                imagen  
                ruta    
                link    
                orden   
                email_empresa   
                tamano  
                alto    
                target  
                lenguaje    
                status  
                encuesta
                */

                Data.get('publicidades').then(function (results) {
                    if (results.status === "success") {
                        $scope.publicidades = results.data;
                    }
                });

                Data.get('promociones').then(function (results) {
                    if (results.status === "success") {
                        $scope.promociones = results.data;
                    }
                });

                $scope.buscador = {
                    ciudad: ''
                };

                $scope.busqueda = function (buscador) {
                    Busqueda.setCiudad(buscador.ciudad);
                    $location.path("/busqueda");
                };

                 Data.get('recientes').then(function (results) {
                    //Data.toast(results);
                    $scope.membresias_recientes = results.data;
                   
                });

                Data.get('getDestacadas').then(function (results) {
                    $scope.membresias_destacadas = results.data;
                });

   }]);
