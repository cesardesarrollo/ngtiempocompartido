
 "use strict";

angular.module('app')
    .controller('publicarCtrl', function ($scope, $rootScope, jwtHelper, store,  $location, $http, Data) {
	
	$scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';

	var token = store.get("token") || null;
	var user = store.get("user") || null;

	console.log(user);


	if(user === null){
		var toast = {
			status: "error",
			message: "Antes debes crear una cuenta y/ó accesar al sistema para publicar una propiedad vacacional."
		}

		Data.toast(toast);
  		$location.path('login');
	}

	var membresia = {};

    $scope.addMembresia = function(membresia) {
		if(user.id !== ""){
	        Data.post('addMembresia', {
	            membresia: membresia
	        }).then(function(results) {
	        	console.log(membresia);
	            Data.toast(results.status,results.mesagge);
	            if (results.status == "success") {

  					$location.path('mismembresias');
	               
	            }
	        });

	 	} else alert("No estas logeado");
	} 

});