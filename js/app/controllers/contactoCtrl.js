
 "use strict";

angular.module('app')
    .controller('contactoCtrl', function ($scope, $rootScope, jwtHelper, store,  $location, $http, Data) {
	
	$scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';

	var token = store.get("token") || null;
	var user = store.get("user") || null;

	  console.log(token);  
	  console.log(user);


	var contacto = {};

	$scope.postContacto = function(contacto){

		if(user.id !== ""){
	        Data.post("postContacto", {
		            contacto: contacto
		        }).then(function(results) {

		        Data.toast(results);

		        if (results.status == "success") {
		        	$scope.contacto = {}
				}

	        });
	  	}

	}



});