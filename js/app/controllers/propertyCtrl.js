$(function(){


    'use-strict';

    var idMem,
        idUser,
        prop_email,
        prop_ciudad,
        prop_iduser,
        images;

    /*global images*/
    angular.module('app')
        .controller('propertyCtrl', ['$scope', '$rootScope', '$stateParams', '$location', '$http', 'Data', 'store','Page',
        function($scope, $rootScope, $stateParams, $location, $http, Data, store, Page) {

        $scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';
        
        var user = store.get("user") || null;


        $scope.agregaComentario = function() {
            if (user === null) {
                var toast = {
                    status: "error",
                    message: "Debes accesar al sistema para realizar esta acción"
                }
                Data.toast(toast);
                return false;
            } else {
                var comentario = ($scope.comentario || null);
                if (comentario !== null) {
                    Data.post('agregacomentario', {
                        idUser: user.id,
                        idMem: idMem,
                        pregunta: $scope.comentario
                    }).then(function(results) {

                        Data.toast(results);
                        if (results.status == "success") {
                            var nuevo = {
                            fecha:  results.fecha,
                            user_id: user.id,
                            user_comentario: $scope.comentario
                            } 
                            $scope.membresia.comentarios.push(nuevo);
                        }
                    });
                } else {
                    var toast = {
                        status: "error",
                        message: "No has escrito un comentario"
                    }
                    Data.toast(toast);
                    return false;
                }
            }
        }

        var imagen = [];

        $scope.contacto = {};

        $scope.enviarContacto = function() {

            if (user === null) {
                var toast = {
                    status: "error",
                    message: "Debes accesar al sistema para realizar esta acción"
                }
                Data.toast(toast);
                return false;
            } else {


                var interes = ($scope.contacto.interes || null);
                if (interes !== null) {
                    var toast = {
                        status: "error",
                        message: "No has llenado todo el formulario"
                    }
                    Data.toast(toast);
                    return false;
                }


                var telefono = ($scope.contacto.telefono || null);
                if (telefono !== null) {
                    var toast = {
                        status: "error",
                        message: "No has llenado todo el formulario"
                    }
                    Data.toast(toast);
                    return false;
                }


                var mensaje = ($scope.contacto.mensaje || null);
                if (mensaje !== null) {
                    var toast = {
                        status: "error",
                        message: "No has llenado todo el formulario"
                    }
                    Data.toast(toast);
                    return false;
                }


                var fullname = ($scope.contacto.fullname || null);
                if (fullname !== null) {
                    var toast = {
                        status: "error",
                        message: "No has llenado todo el formulario"
                    }
                    Data.toast(toast);
                    return false;
                }


                var email = ($scope.contacto.email || null);
                if (email !== null) {
                    var toast = {
                        status: "error",
                        message: "No has llenado todo el formulario"
                    }
                    Data.toast(toast);
                    return false;
                }



                var titulo = "Interesado en " + interes + " de tu tiempo compartido.";
                var cuerpo = "Datos del interesado. Nombre: " + fullname + ", Teléfono: " + telefono + ". Mensaje: " + mensaje;
                var contacto = {
                    'titulo': titulo,
                    'cuerpo': cuerpo,
                    'id_envia': user.id,
                    'id_recibe': prop_iduser,
                    'categoria':'pregunta',
                    'email_envia': email,
                    'email_recibe': prop_email,
                    'idMem': idMem
                }
                Data.post("enviarcontacto",contacto).then(function(result) {
                    console.log("Enviar forma de contacto");
                    console.log(result);
                });
            }
        }



        $scope.getPropiedad = function(propiedad) {
            Data.get("propiedad/" + propiedad.idMem).then(function(results) {
                console.log(results);
                //Data.toast(results);
                if (results.status == "success") {

                    Page.setTitle(results.data.enlace_des);

                    idMem       = results.data.idMem;
                    prop_email  = results.data.usuario.email;
                    prop_ciudad = results.data.ciudad;
                    prop_iduser = results.data.usuario.id;


                    $scope.membresia = results.data;
                    console.log( $scope.membresia);
                    //RELACIONADAS
                    $scope.relacionadas();

                    //Mapa
                    var cities = [{
                        city: results.data.ciudad,
                        desc: results.data.informacion,
                        lat: results.data.googlemaps.latitude,
                        long: results.data.googlemaps.longitude
                    }];
                    var mapOptions = {
                        zoom: 18,
                        center: new google.maps.LatLng(results.data.googlemaps.latitude, results.data.googlemaps.longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                    $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                    $scope.markers = [];
                    var infoWindow = new google.maps.InfoWindow();
                    var createMarker = function(info) {
                        var marker = new google.maps.Marker({
                            map: $scope.map,
                            position: new google.maps.LatLng(info.lat, info.long),
                            title: info.city
                        });
                        marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';
                        google.maps.event.addListener(marker, 'click', function() {
                            infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                            infoWindow.open($scope.map, marker);
                        });
                        $scope.markers.push(marker);
                    }
                    for (i = 0; i < cities.length; i++) {
                        createMarker(cities[i]);
                    }
                    $scope.openInfoWindow = function(e, selectedMarker) {
                        e.preventDefault();
                        google.maps.event.trigger(selectedMarker, 'click');
                    }

                }
            });
        };
        $scope.getPropiedad($stateParams);

    
        $scope.relacionadas = function() {

            Data.post('relacionadas',{
            ciudad: prop_ciudad
            }).then(function(results) {
                $scope.relacionadas = results.data;
            });
        }

    }]);






    
})();
