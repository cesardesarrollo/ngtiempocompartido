
 "use strict";

angular.module('app')
    .controller('mismembresiasCtrl', function ($scope, $rootScope, jwtHelper, store,  $location, $http, Data) {
	
	$scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';

	var token = store.get("token") || null;
	var user = store.get("user") || null;

	console.log(user);

    $scope.poblateMembresias = function(idUser) {
		if(user.id !== ""){
	        Data.get('poblateMembresias/'+idUser).then(function(results) {

		        console.log(results);

	            if (results.status == "success") {
                	$scope.membresias = results.data;
	               
	            } else {

	            	$scope.showAlertNotFound = true;
	            }
	        });

	 	} else alert("No estas logeado");
	}
	$scope.poblateMembresias(user.id);


  	$scope.openeditMembresia = function(idMem) {
  		$location.path('editMembresia/'+idMem+"/");
	} 

  	$scope.uploadImagen = function(idMem) {
  		$location.path('uploadImage/'+idMem+"/");
	} 


  	$scope.uploadUbication = function(idMem) {
  		$location.path('uploadUbication/'+idMem+"/");
	} 

    $scope.deleteMembresia = function(idMem) {
		if(user.id !== ""){

			var confirma = confirm("Deseas realmente eliminar esta propiedad?");
			if (confirma) {

		        Data.post('deleteMembresia/'+idMem).then(function(results) {
		        	
		        	console.log(results);

		            Data.toast(results.status,results.mesagge);
		            if (results.status == "success") {
		               
						$scope.poblateMembresias(user.id);
						
		            }
		        });

			}

	 	} else alert("No estas logeado");
	} 


});