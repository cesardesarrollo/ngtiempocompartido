
 "use strict";

angular.module('app')
    .controller('perfilCtrl', function ($scope, CONFIG, $rootScope, jwtHelper, store, upload, $location, $http, Data) {
	
	$scope.app.settings.htmlClass = 'hide-sidebar top-navbar ls-bottom-footer-fixed';

	var token = store.get("token") || null;
	var user = store.get("user") || null;

	  console.log(token);  
	  console.log(user);

	if(user.id !== ""){
        Data.get("perfil/" + user.id).then(function(results) {
	        if (results.status == "success") {
				$scope.perfil = results.data[0];
	        	console.log(results.data[0]);
			}

        });
  	}

    $scope.perfil = {
        email: '',
        password: '',
        nombre: '',
        informacion: '',
        telefono: '',
        pais: '',
        ciudad: '',
        propietario: '',
        destinos: '',
        perfil: '',
        fotoperfil: ''

    };
    $scope.editPerfil = function(data,id) {
        Data.put('editPerfil/' + id, {
            data
        }).then(function(results) {
            Data.toast(results.status,results.mesagge);
            if (results.status == "success") {
               alert("correcto");
            }
        });
    };



    $scope.uploadFile = function(file,id)
    {
               console.log(file);
               console.log('id');
        var file = $scope.file;

        if (file === undefined) {
            alert("No has elegido imagen");
            return false;
        }

        
        upload.uploadFile(file, id).then(function(res) {
            Data.toast(res.data);
            
            console.log(res);

        })
    }

})

.directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        require: '^ngModel',
        link: function (scope, iElement, iAttrs) 
        {
/*
            ngModel.$viewChangeListeners.push(function(){
                console.log('changed');
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
*/
            iElement.on("change", function(e)
            {


     console.log('changed2');
                var file = $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            

     console.log(file);





            });
        }
    };
}])

.service('upload', ["$http", "$q", "CONFIG", function ($http, $q, CONFIG) 
{
    this.uploadFile = function(file, id)
    {
        var deferred = $q.defer();
        var formData = new FormData();
        formData.append("file", file);


        return $http.post(CONFIG.APIURL + "uploadAvatar/" +  id, formData, {
            headers: {
                "Content-type": undefined
            },
            transformRequest: angular.identity

        })
        .success(function(res)
        {
            deferred.resolve(res);
        })
        .error(function(msg, code)
        {
            deferred.reject(msg);
        })
        return deferred.promise;
    }   
}]);